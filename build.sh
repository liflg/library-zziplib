#!/bin/bash

DetermineArchitecture()
{
    ret=0
    case `uname -m` in
    x86_64)
        echo "x86_64";;
    i?86)
        echo "i386";;
    *)
        echo "Unsupported machine architecture."
        ret=1;;
    esac
    return ${ret}
}

DetermineOperatingSystem()
{
    ret=0
    case `uname -s` in
    Linux)
        echo "linux-gnu";;
    *)
        echo "Unsupported operating system."
        ret=1;;
    esac
    return ${ret}
}

arch=`DetermineArchitecture`
ret=$?
echo ${arch}
if [ ${ret} -ne 0 ]
then
    exit ${ret}
fi

os=`DetermineOperatingSystem`
ret=$?
echo ${os}
if [ ${ret} -ne 0 ]
then
    exit ${ret}
fi

if [ ${CC} ]
then
        export CC="$CC -ggdb"
else
        export CC="gcc -ggdb"
fi

if [ ${CXX} ]
then
        export CXX="$CXX -ggdb"
else
        export CXX="g++ -ggdb"
fi

MULTIARCHNAME=${arch}-${os}
BUILDDIR="build_${MULTIARCHNAME}"
LIBSDIR="`dirname $PWD`"

PREFIXDIR="${PWD}/${MULTIARCHNAME}"
echo "Install prefix: \"${PREFIXDIR}\""

mkdir ${BUILDDIR} &&
cd ${BUILDDIR} &&
../source/configure \
  --prefix="$PREFIXDIR" \
  --disable-static &&
make &&
make install &&
cp ../source/docs/copying.htm "$PREFIXDIR"/lib/LICENSE-zziplib.txt &&
cd .. &&
rm -rf "${BUILDDIR}" &&
echo "Build for ${MULTIARCHNAME} is ready." ||
echo "Build failed"
