Website
=======
http://zziplib.sourceforge.net/

License
=======
GNU Lesser General Public License version 2 or later
or
Mozilla Public

(have a look at the file source/docs/copying.htm)

Version
=======
0.13.59

Source
======
http://switch.dl.sourceforge.net/project/zziplib/zziplib13/0.13.59/zziplib-0.13.59.tar.bz2
